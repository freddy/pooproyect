package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    TextView PRECIO, NOMBRE,DESCRIPCION;
    ImageView IMAGEN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6
        // Crea una instancia de la clase Intent para llamar a la activity DelailActivity y pasa
        //como parametro el m_objectID del DataObject

        String object = getIntent().getStringExtra("object_id");
        PRECIO = (TextView) findViewById(R.id.precio);
        NOMBRE = (TextView) findViewById(R.id.nombre);
        DESCRIPCION = (TextView) findViewById(R.id.descripcion);
        IMAGEN =  (ImageView) findViewById(R.id.imagen);

        //Accede a las propiedades del object de tipo String: name,price,description e imagen


        DataQuery query =DataQuery.get("item");
        String parametro = getIntent().getExtras().getString("objecto1");
          // Recibe el parametro añadido en el Layout activity_detail.xml

        query.getInBackground(parametro, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e==null){

                    String Precio1 = (String) object.get("price") + ("\u0024");
                    String Descripcion1 = (String) object.get("description");
                    String Nombre1 = (String) object.get("name");
                    Bitmap ImagenBitmap = (Bitmap) object.get("image");
                        //Se guardan los datos

                    PRECIO.setText(Precio1);
                    DESCRIPCION.setText(Descripcion1);
                    NOMBRE.setText(Nombre1);
                    IMAGEN.setImageBitmap(ImagenBitmap);

                }else  {
                    //ERROR
                    // FIN - CODE6

                }
            }
        });




    }

}
